---
title: 'Prior notice to using PDFD'
excerpt: "A few things to consider before using Prot's Dots For Debian."
flag: pdfd
layout: pdfd
permalink: /pdfd/prior-notice/
---

The purpose of _Prot's Dots For Debian_ (PDFD) is to guide you through
the process of replicating my custom desktop session on Debian 10
'buster'.

I have tried every step in this guide on real hardware, my Lenovo
ThinkPad X220.  The initial installation was done on Saturday 12 January
2019 using the latest netinstall iso for Debian 9 'stretch' and retried
on 2019-04-17 using the same method.

COPYING
-------

All code by Protesilaos Stavrou presented herein is available under the
terms of the GNU General Public License Version 3 or later.

The non-code parts of this book by Protesilaos Stavrou are distributed
under the terms of the Creative Commons Attribution-ShareAlike 4.0
International License.

The canonical link to "Prot's Dots for Debian" is an extension of my
website: https://protesilaos.com/pdfd

DISCLAIMERS
-----------

The following disclaimers apply to this work.

Excerpt from the GPLv3:

>THERE IS NO WARRANTY FOR THE PROGRAM, TO THE EXTENT PERMITTED BY
>APPLICABLE LAW. EXCEPT WHEN OTHERWISE STATED IN WRITING THE COPYRIGHT
>HOLDERS AND/OR OTHER PARTIES PROVIDE THE PROGRAM “AS IS” WITHOUT
>WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT
>LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
>A PARTICULAR PURPOSE. THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE
>OF THE PROGRAM IS WITH YOU. SHOULD THE PROGRAM PROVE DEFECTIVE, YOU
>ASSUME THE COST OF ALL NECESSARY SERVICING, REPAIR OR CORRECTION.

And this is from the CC BY-SA 4.0 License:

>a. Unless otherwise separately undertaken by the Licensor, to the extent
>possible, the Licensor offers the Licensed Material as-is and
>as-available, and makes no representations or warranties of any kind
>concerning the Licensed Material, whether express, implied, statutory,
>or other. This includes, without limitation, warranties of title,
>merchantability, fitness for a particular purpose, non-infringement,
>absence of latent or other defects, accuracy, or the presence or absence
>of errors, whether or not known or discoverable. Where disclaimers of
>warranties are not allowed in full or in part, this disclaimer may not
>apply to You.
>b. To the extent possible, in no event will the Licensor be liable to
>You on any legal theory (including, without limitation, negligence) or
>otherwise for any direct, special, indirect, incidental, consequential,
>punitive, exemplary, or other losses, costs, expenses, or damages
>arising out of this Public License or use of the Licensed Material, even
>if the Licensor has been advised of the possibility of such losses,
>costs, expenses, or damages. Where a limitation of liability is not
>allowed in full or in part, this limitation may not apply to You.
>c. The disclaimer of warranties and limitation of liability provided
>above shall be interpreted in a manner that, to the extent possible,
>most closely approximates an absolute disclaimer and waiver of all
>liability.

