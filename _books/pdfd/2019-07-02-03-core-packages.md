---
title: 'Installing the core packages'
excerpt: 'Instructions to install the necessary packages for the custom desktop session.'
flag: pdfd
layout: pdfd
permalink: /pdfd/install-core-packages/
---

Boot up your newly-installed Debian 10 'buster'.  When you reach the
display manager screen, enter your user credentials (user name and
password).  This will put you in a default MATE session (pronounced
_MA-te_, from _yerba maté_).  From here, we will be installing all the
packages we need and doing the rest of the preparatory work.

## Enable sudo

First things first: we need to grant superuser privileges to your user
account.  This is necessary in order to use `sudo` with the various
commands, rather than having to switch to the root user.

Open a terminal.  For this initial step, you need to switch to the root
account.

	su -

While being root, make sure you update the package lists.  Then install
Vim and the package that enables "sudo".

	apt update && apt install vim sudo

Also install these core utilities, which are needed for getting and
deploying my dotfiles.

	apt install git stow

Once that is done, add your user account to the "sudo" group, replacing
USER with your username.

	adduser USER sudo

Reboot if you want to use the regular user, or continue as root.  For
the sake of this manual, I assume you rebooted, logged back in to the
default MATE session and are now prepared to run all commands from your
regular user but with escalated privileges, prepending "sudo" to all
relevant commands.

## Getting BSPWM and related core components

We start by installing:

* the window manager (`bspwm`) as the irreducible factor of my custom
  desktop session,
* the hotkey daemon (`sxhkd`) for handling custom key bindings that
  control BSPWM and other programs,
* the standard terminal emulator `xterm`,
* the `suckless-tools`, which provide the simple screen locker (`slock`)
  and the dynamic menu (`dmenu`),
* the program that manages the wallpaper and can display images (`feh`),
* the display compositor (`compton`) for smoother transitions and no
  screen-tearing,
* the notification daemon and concomitant library for sending desktop
  notifications (`dunst` and `libnotify-bin` respectively),
* the system bar (`lemonbar`) that is used by one of my scripts to draw
  a bespoke panel on the top of the viewport as well as its dependencies
  (`xdo`, `acpi`),
* and the secrets' manager (`gnome-keyring`).

Run this:

	sudo apt install bspwm sxhkd xterm suckless-tools feh compton dunst libnotify-bin lemonbar xdo acpi gnome-keyring

Note that `slock` may be unintuitive at first, because it just turns the
screen dark without any further notice.  You unlock it by typing in your
pass word (confirming with "Return").  The screen will keep switching to
a blue colour as you type.  I am aware this is not the most
user-friendly design, at least not for first time users, but I decided
to keep it nonetheless: once you know about it, it works just fine.  If
you find yourself disliking this tool, consider installing Debian
package `i3lock`: it is a bit more intuitive and configurable (then you
need to apply changes to my script `poweroptionsmenu`—refer to the
chapter about my local ~/bin).

To make sure `dunst` works unencumbered, we better remove MATE's own
notification daemon:

	sudo apt remove mate-notification-daemon

_For more on this set of packages, see the chapter about the basics of my
BSPWM as well as the one about the top panel._

## GTK icon theme

Now we get the GTK icon theme.  I choose Papirus because it is very well
crafted and actively developed.

	sudo apt install papirus-icon-theme

## Fixed and proportional fonts

We need outline/proportional typefaces for the various UI components and
graphical applications, plus a fixed-size (bitmap) typeface for use in
the system panel.

The outline fonts are:

	sudo apt install fonts-firacode fonts-hack fonts-roboto fonts-dejavu fonts-dejavu-extra fonts-noto-color-emoji fonts-symbola

The bitmap font is Terminus.  This is optional, though highly
recommended:

	sudo apt install xfonts-terminus

_The typographic considerations are discussed in the chapter about the
fonts and their configs._

## Terminal tools

My user session makes heavy use of a terminal multiplexer (`tmux`),
while I occasionally need to use Vim's external register or graphical
application (`vim-gtk3`).

	sudo apt install tmux vim-gtk3

_The use of these tools is documented in the chapter about my Tmux and
Vim combo.  Of relevance is the chapter about the default terminal
setup._

## General CLI tools

Then we need the RSS/Atom feed reader for the console (`newsboat`) and
a simple utility to capture screenshots (`scrot`).

	sudo apt install newsboat scrot

Are you using a laptop or a screen with built-in brightness controls?
You need this:

	sudo apt install xbacklight

## The music setup

For my music, I choose to use the Music Player Daemon and connect to it
with a client program (`mpc` and/or `ncmpcpp`).  I also want MPD to
expose itself to applications that implement the MPRIS protocol
(`mpdris2` with `python-mutagen` for album covers), in order to be able
to control it through other means than its own clients (`playerctl`). To
this end, I install the following:

	sudo apt install mpd mpc ncmpcpp mpdris2 python-mutagen playerctl

_Detailed instructions about these are provided in the chapter about the
Per-user MPD setup._

## Extra packages for convenience and added functionality

I do not want my BSPWM session to be primitive.  I just want it to be
configurable and catered to my needs, while being light on system
resources.

I therefore need a multimedia player that I can launch from the console,
which also streams online content (`mpv` with `youtube-dl`), a tool that
can edit/transform/convert images from the command line (`imagemagick`),
a program to perform file transfers (`rsync`), a graphical frontend to
my password manager (`qtpass`, which pulls in `pass`), a frontend for
PulseAudio (`pavucontrol`), a very capable image viewer (`sxiv`), and
a calculator for the console (`apcalc`—the executable is `calc`).

	sudo apt install mpv youtube-dl imagemagick rsync qtpass pavucontrol sxiv apcalc

## Thunderbird setup (optional, but recommended)

In the same spirit, I use Thunderbird as my primary email client (I also
use Mutt, which is not covered in this book due to its configuration
being highly dependent on the user).  Thunderbird is a robust tool that
can easily filter spam, handle my CalDAV and CardDAV accounts, and cope
with large volumes of email traffic.  The following command will get you
the email client, plus extensions for GPG encryption (`enigmail`) and
calendaring (`lightning`).

	sudo apt install thunderbird enigmail lightning

While still on the topic of Thunderbird, I also install the following
package for handling {Cal,Card}DAV services.  I put it here on its own
as you might have no need for it, unless your host also uses SOGo.

	sudo apt install xul-ext-sogo-connector

For further language support, I also get these (the latter with
extension `-el` is for the Greek language):

	sudo apt install hunspell {a,hun}spell-el

## Firefox setup (optional, but recommended)

The Extended Support Release of Firefox (`firefox-esr`) is my web
browser of choice.  This is shipped by Debian as the default option.
Users normally install whatever web extensions they may need via the
browser's own interface.  However, I have found that I prefer to let
`apt` handle things.  The following packages are sufficient for my
use case:

	sudo apt install webext-noscript webext-https-everywhere webext-ublock-origin webext-privacy-badger

These extensions are NoScript, HTTPS Everywhere, UBlock Origin, Privacy
Badger.

To make the browser better suited to my needs, I also disable the Pocket
bloatware, from inside Firefox's interface.

* First type the address `about:config` and accept the warning message.
* Then search for the entry `extensions.pocket.enabled`.
* Double click to change it to false.

Then we need to address an age-old bug that affects dark GTK themes
where Firefox may display dark text on a dark action element, like
a search box.

* Visit `about:config`.
* Do: left click on some empty space > select New > select String.
* Add this: `widget.content.gtk-theme-override`.
* Its value should be `Adwaita` (the default GTK light theme).

## Optional packages

Sometimes I need to edit photographs (`darktable`), record audio
(`audacity`), and use a different web browser (`epiphany-browser`).

	# other packages
	sudo apt install audacity darktable epiphany-browser

And here are some other console tools that might come in handy.  `vrms`
displays information about non-free software on your system, while
`neofetch` prints details about your machine and distro.

	sudo apt install vrms neofetch

I just ran `vrms` and it tells me that I have 1 non-free package on my
ThinkPad X220 out of a total of 1727.  ABSOLUTELY PROPRIETARY!  The
offending package is `firmware-iwlwifi` for making Wi-Fi work, else
I cannot access the Internet…

## Optional: set up Flatpak with Flathub as its remote repo

I think that, when used in moderation and care, Flatpak is a compelling
proposition for a stable OS like Debian.  Your system will remain rock
solid, while the Flatpak'ed applications will use their latest version,
while being confined to a sandboxed environment (in the near future
a tool like `guix` may be the superior alternative).

To get started, install the core package:

	sudo apt install flatpak

Now add [Flathub as a remote package repository](https://flathub.org).
Note that you can have multiple remote repos.  This is something that,
in my opinion, distros should provide themselves (would much rather
trust Debian's curated list of `flatpak` packages, but I digress).

	flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

For the record, Flatpak needs a running settings daemon to known which
theme to apply.  It is why I use the `mate-settings-daemon` in my BSPWM
session (more on that in the chapter about the basics of BSPWM).

Note that Flathub does not discriminate against non-free software.  If
you care about freedom, exercise caution.  That granted, the only
flatpaks I tend to use are libre:

	flatpak install org.gnome.Podcasts org.gnome.clocks org.kde.kdenlive

## Next steps

We are done installing software.  Thanks for your patience!  Let us
proceed to the next chapter where we actually get the dotfiles and stow
them in place.

_Stay logged in to the current MATE session for the time being and keep
reading_.

