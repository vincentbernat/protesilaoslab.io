---
title: 'Closing notes and additional resources'
excerpt: 'Concluding remarks with links to more resources that are relevant to my dotfiles and this book.'
flag: pdfd
layout: pdfd
permalink: /pdfd/additional-resources/
---

That is all from my side.  Thank you for reading through the pages of
_Prot's Dots for Debian_ (PDFD).  You should now have a solid basis to
build upon.

Remember: we customise our session to improve our workflow.  Efficiency
is all that matters.  What gets upvoted in various fora for \*NIX
enthusiasts does not always work as well (e.g. low-contrast colour
schemes, excessive transparency and wanton use of blur effects).

I leave you with some links, just for the sake of completeness:

* The canonical link to [PDFD](https://protesilaos.com/pdfd) (part of my
  website).
* [My website](https://protesilaos.com/), where I publish all sorts of
  things apart from libre software stuff.
* Source files for _Prot's Dots For Debian_:
	1. [PDFD repo](https://gitlab.com/protesilaos/pdfd).
	2. [Site repo](https://gitlab.com/protesilaos/protesilaos.gitlab.io).
* The [Code for PDFD](https://gitlab.com/protesilaos/cpdfd) contains the
  latest fixed release of my dotfiles.  It was our reference point
  throughout this book.
* [My dotfiles](https://gitlab.com/protesilaos/dotfiles) is where I push
  regular changes to my custom desktop session.  I include them in
  a "fixed release" when I feel they are ready for broader adoption.
* [Tempus themes](https://gitlab.com/protesilaos/tempus-themes).  This
  is just the main repo, which includes information about the entirety
  of the project, as noted in the chapter about the Tempus themes.

