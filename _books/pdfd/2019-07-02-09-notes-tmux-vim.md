---
title: 'Notes about my Tmux and Vim'
excerpt: 'An overview of the two most used tools in my custom desktop session (the BSPWM parts notwithstanding).'
flag: pdfd
layout: pdfd
permalink: /pdfd/notes-tmux-vim/
---

Both Tmux and Vim are essential to my workflow.  It is thus pertinent to
inform you about their respective configuration files so that you know
what to expect.  This is with the proviso that you already are familiar
with these tools.

## About my tmux setup

Let's start with the terminal multiplexer.  It is the first thing you
will interact with when you log into the session and type the key chord
`super + return`:

	~/cpdfd $ tree -aF tmux
	tmux
	└── .tmux.conf

	0 directories, 1 file

Only its config is distributed with my dotfiles, which is another way of
saying that _I do not use any plugins whatsoever_.  What is provided by
the program itself is more than enough.

To send commands to `tmux` you must typically start by first pressing
its prefix key, which I have kept to the default of `Ctrl + b` as it is
the one that causes no issues with other programs' main functionality,
_in the way I use them_.  To be clear, `Ctrl + b` does interfere with
a couple of commands: (a) the command line motion for moving one
character backwards and (b) Vim's full page back scroll.

So you press the prefix key, then release it and you can then either
access the command prompt by inputting the colon `:` sign or typing one
of the many key sequences that are assigned to direct actions (see table
below).

In the configuration file, modifier keys are shortened.  So `Ctrl + b`
is written as `C-b`, `Alt` is `A`.  Note that `Shift` is not used
directly, but is instead implied by the use of a capital letter (just to
be sure: Shift inserts the majuscule of the given letter).

Here are the main key bindings to get you started:

	# Those that involve C-b (prefix)
	# -------------------------------
	<prefix> s        # split current pane horizontally
	<prefix> v        # split current pane vertically
	                  ### these are similar to Vim's C-w {s,v}
	<prefix> S        # split the whole window horizontally
	<prefix> V        # split the whole window vertically
	<prefix> x        # kill current pane
	<prefix> C-x      # kill all panes except the current one
	<prefix> r        # reload the tmux conf
	<prefix> m        # mark active pane
	<prefix> C-m      # toggle maximise active pane
	                  ### the default is <prefix> z (too close to x)
	<prefix> A-{1-5}  # switch to one of the five layout presets
	<prefix> E        # evenly spread out active and adjacent panes
	<prefix> c        # create a new window (windows contain panes)
	<prefix> b        # break pane from current window
	                  ### default is <prefix> !
	<prefix> J        # the opposite of break-pane
	<prefix> Tab      # swap active pane with the previous one
	<prefix> C-Tab    # swap active pane with the marked one
	<prefix> a        # sync input across panes in the current window

	# Keys without the prefix
	# -----------------------
	A-{h,j,k,l}       # navigate panes in the given direction
	A-S-{h,l}         # move to left/right window
	C-Space           # enter copy-mode
	                  ### use Vim keys to scroll, select/copy text

Read `.tmux.conf` for the entirety of my settings and custom key
bindings.  That file is heavily documented, as is the norm with
practically every item in my dotfiles.

Now if you are thinking that you do not need a terminal multiplexer and
have no time to learn how to use one, I urge you to think again.  This
is a great skill to master.  It greatly improves the overall use of the
terminal.  It is a skill that is highly portable.  It will come in handy
in a variety of situations whereas, to be blunt, learning `bspwm` may
not be particularly useful outside the narrow confines of a custom
desktop session.

Tmux is superior to a standard terminal because it offers unique
capabilities:

* Persistent local/remote sessions (even if you close the terminal or
  log out).  Once you use this, there is no going back.
* Advanced management of a large number of pseudo-terminals by
  leveraging window splitting (panes) like a tiling window manager, pane
  grouping per window (the equivalent of tabs/workspaces), and sessions
  (sessions hold windows, windows hold panes).
* Scriptability including the possibility to send key sequences to
  running applications (I use this to source my .vimrc when I perform
  a live theme change, as noted in the chapter about my Tempus themes).

## About my Vim setup

That last piece of advice holds true for `vim` as well.  Spend some time
learning its basics.  Over time you will become proficient at editing
text.

And here is another piece of advice, before we delve into the specifics
of my Vim setup: set a very high bar for the use of plugins or, as in my
case, do not use plugins _at all_.  Also avoid copy-pasting stuff
without carefully considering the ramifications.

The more comfortable you are with the generic program, the higher the
portability of your knowledge.  Vim is meant to be used as the standard
UNIX editor that is available in virtually every such machine out there.
Straying too much from the defaults might impede your ability to work
effectively under circumstances that are not under your immediate
control.

Now on to my customisations.

	~/cpdfd $ tree -aF vim
	vim
	├── .vim/
	│   ├── colors/
	│   │   ├── tempus_autumn.vim
	│   │   ├── tempus_classic.vim
	│   │   ├── tempus_dawn.vim
	│   │   ├── tempus_day.vim
	│   │   ├── tempus_dusk.vim
	│   │   ├── tempus_fugit.vim
	│   │   ├── tempus_future.vim
	│   │   ├── tempus_night.vim
	│   │   ├── tempus_past.vim
	│   │   ├── tempus_rift.vim
	│   │   ├── tempus_spring.vim
	│   │   ├── tempus_summer.vim
	│   │   ├── tempus_tempest.vim
	│   │   ├── tempus_totus.vim
	│   │   ├── tempus_warp.vim
	│   │   └── tempus_winter.vim
	│   └── spell/
	│       ├── el.utf-8.spl
	│       ├── el.utf-8.sug
	│       ├── en.utf-8.add
	│       └── en.utf-8.add.spl
	└── .vimrc

	3 directories, 19 files

A quick rundown of my very short and simple `.vimrc`:

* I stick to the default key bindings.
* I expect Vim to ask for confirmation when closing a modified buffer.
* I use tabs instead of spaces for indentation, setting the width of the
  tab character to be equal to four spaces.  I used to use spaces when
  I first started using a text editor, because that was the default.
  However, experience suggests that tabs are semantically more
  appropriate for indentation.  The tab key inserts its own character,
  which can have an arbitrary width defined by the program that reads
  it.  In short, tabs are better for indentation, while spaces are
  better for tabular layouts such as the `tmux` key table I presented
  above.
* The text width is 72 blocks long.  This is particularly important for
  writing good `git` commit messages.  And, because `git` is designed
  with email in mind, this line length is ideal for sending plain text
  email, such as with `mutt`.  In general, this line length also makes
  sense for comment blocks in your code, because it makes them easier to
  read without having to rely on non-standard things like text-wrapping
  (so, for example, hitting `gqip` will format inside the given
  paragraph, while `gq` does the same over the given selection).
* Sentences in a paragraph start with two spaces after a period or full
  stop.  This is better visually when typing in a monospaced font.  The
  various parsers, e.g. Markdown to HTML, know how to convert that to
  a single space, just as they know how to turn hard line wraps between
  consecutive lines of text into uniform paragraphs (a blank line marks
  a new paragraph).
* Syntax highlighting is on and uses my Tempus themes.  Do not edit this
  part manually, as it will be changed when running `tempus` or its
  `tempusmenu` interface (see chapter on Tempus themes).

Read the `.vimrc` for an in-depth understanding of my customisations (it
is a straightforward, well-commented config).

## Neither Tmux nor Vim is an OS

Now you may be wondering how it is possible to use Tmux and Vim without
plugins and all the accoutrements of a modern workflow.  The answer is
rather simple: let Tmux+Vim be specialised tools and leave the rest to
other programs.

Do you really need Vim's sub-par approach to multiplexing (its own
approach to splits, buffers, tabs) when you can just be a `tmux` ninja?
What, you need more than that?  Let me tell you about this nice program
called BSPWM…  Why do you require a plugin to check `git` information
inside Vim when you can just use the command line?  Open a new `tmux`
split and type `git status`, `git log`, `git diff`…  You need to commit
only parts of a changed file?  Know that `git add --patch` is your
friend.

In the same spirit, use the core utilities like `cd`, `ls`, `find`,
`grep`, `cp`, `mv`, `rm`.  Keep things simple and avoid feature creep.
Let the text editor _edit text_.  Keep the multiplexer true to its
spirit of managing terminal sessions.  If you truly need an extensible,
fully-customisable integrated development environment, then you should
seriously consider GNU Emacs (which is a Lisp interpreter that
implements a text editor among _many_ others).

Of course, there are scenaria where a plugin makes a program better for
the task at hand.  My point is that you should be very picky about your
choice of external functionality.  GNU/Linux is a powerful OS (Emacs
too!).  You do not need to incorporate every kind of feature in the core
tool.  Perhaps there are other operating systems that make things
difficult for the power user: if you are using one of those, then
a pile of plugins is the least of your troubles.

As a closing remark, let me leave you with a joke I once heard about
Emacs (a tool that I genuinely like and will eventually incorporate in
my computing life): its name is an allusion to its design that involves
active use of modifier keys…  Escape, Meta, Alt, Control, Shift.

