---
title: "Overgrowth theme"
date: 2016-06-14
excerpt: "Medium-to-high contrast theme with a vibrant look, inspired by tropical vegetation."
permalink: /prot16-overgrowth/
redirect_from: /overgrowth/
---
*Overgrowth* is a medium-to-high contrast theme inspired by tropical vegetation. It comes in light and dark variants, while using a single 16-colour palette.

{% include prot16-project-template.md %}
