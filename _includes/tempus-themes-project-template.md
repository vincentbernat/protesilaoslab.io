{% assign themes = site.data.tempus-themes %}

{% for theme in themes %}
{% if page.permalink contains theme.slug %}

{{ theme.description }}.

<img alt="{{ theme.slugunderscore | prepend: 'Image sample: ' }}" src="{{ theme.slugunderscore | prepend: '/assets/images/tempus/' | append: '.png' | absolute_url }}"/>

## {{ theme.name }} Palette

{% include tempus-themes-palette.html %}

* * *

## About the Tempus Themes project

Tempus is a collection of themes for Vim, text editors, and terminal emulators that are compliant at *the very least* with the WCAG AA accessibility standard for colour contrast. The contrast between foreground and background values is >= 4.50:1 or >= 7.00:1. The idea is to provide consistent colour palettes, while preserving legibility.

## Tempus themes repositories

Each repo has instructions on how to use the themes for the application of your choice.

{% include tempus-themes-repos.html %}

## GNU/Linux packages

Tempus themes are available for Arch Linux and derivatives, via the
AUR.  Packages correspond to the following application software:

{% include tempus-themes-packages.html %}

Special thanks to maintainer _yochananmarqos_!  Last reviewed on
2019-10-29.

## Contributing

The project revolves around the Tempus themes generator. This is the tool that contains the colour scheme specs and the application templates. See its [CONTRIBUTING.md](https://gitlab.com/protesilaos/tempus-themes-generator/blob/master/CONTRIBUTING.md).

## License

GNU General Public License Version 3.

## Meta

The project is maintained with a set of helper scripts and utilities that streamline the deployment and maintenance of the git repos. Those interested can refer to the [tempus-themes-utils repository](https://gitlab.com/protesilaos/tempus-themes-utils).

The screenshot on this page is an ad hoc demo within my custom `bspwm` desktop session. The Tempus Themes are a core part of it. See [my dotfiles](https://gitlab.com/protesilaos/dotfiles).

{% endif %}
{% endfor %}
