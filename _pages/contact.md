---
title: Contact me
excerpt: "Information for contacting Protesilaos Stavrou."
permalink: /contact/
layout: page
---

- Email: <public@protesilaos.com>

Feel free to send me your message regarding the content of this website
(blog posts, coding stuff, books, etc.).  I normally reply within 48
hours (usually much sooner).  If you think you have not received a
message from me after a reasonable amount of time, check your "spam"
folder.

You do not need permission to reuse or adapt my work.  Just do it under
copyleft terms.  See the [license page](https://protesilaos.com/license/).

I do not use social media.

## PGP encryption for emails (optional)

Below is my public key.  Save it to a file, say `prot`, open a terminal
emulator and then run the command `gpg --import prot` (or whatever
equivalent for your interface/context).

Make sure to **send me your public key**, else I will not be able to
reply to you.
{:.info}


```
-----BEGIN PGP PUBLIC KEY BLOCK-----

mQENBFkyQvkBCADmjpu9/KnKErtaIfmKs1xuM7aSmF+DJYzult2MQDdsea+pWGyi
c2puFBS4Iq6Y+DjN18xryooQppmctKciwyIZ4H23Q62W6aWC+/fdnPw0OJex4cKe
Wn9IZ7HZrOZOXFOWG6a5pf+TeS4lH3FtWw5MsHpvHtW74Kqmm3i4DgtDP2TYWw41
9nKIUNPFCvr/CAwYgB2cQQxdkhxC4c5xbbs1uFNgQNkzY9nOL1c5ypaH6J/lokMa
EWhZTbY0AzFw5TyHIzt36qEkMPKOj+iSZ547dLSEyCu6JrMsdFlE0eb1mihMMVTI
gOcVswrjcSZN40zwDkYK+J6xKX9gHTZYCaGdABEBAAG0LFByb3Rlc2lsYW9zIFN0
YXZyb3UgPHB1YmxpY0Bwcm90ZXNpbGFvcy5jb20+iQFUBBMBCAA+AhsDBQsJCAcC
BhUICQoLAgQWAgMBAh4BAheAFiEEpbihbYat7LIfuNSIv0j9hP4h1yoFAlzZffAF
CQdpofcACgkQv0j9hP4h1yqDOAf/Sb41QpnHct7IVGpfqHjF203pX80NAd5Ecke7
dS4JbXFjfWvOGzXwP7S18q5Ddzp19Fa+oOHzkHoEV6Ll6ogHdFQgoz3ThQ++V+oY
7cnc1exca76/UuZUYsW7SjktYToA69f1VtzxQRw0GgPH3SVOe3mXNtauj7rEhw03
hgJdlIZvUO1IaZbjpeQaazs9CbD77h4XXkglyk6AZXVylRkxh5p8DaGgZZB5UogV
KMs+9fIZ+ouHWBREnFeJ+1evO/kl7C9oJsw5B82IAiUK4lEWRSRBxewep56Silqh
/KzkVDLyUmnNFE8LC+7abrXTFt9G+rvbu13tvb7Oe53wObWJ2IkBVAQTAQgAPhYh
BKW4oW2GreyyH7jUiL9I/YT+IdcqBQJZMkL5AhsDBQkDwmcABQsJCAcCBhUICQoL
AgQWAgMBAh4BAheAAAoJEL9I/YT+IdcqCIYH/3p/FvMIhwC52+I0Dwqh0iU9BjaD
Sy9/rlr9HOwtfkqiNN9D/DZxtA83WHXhKqdJ2Oqk7EOwORh/bl8TC4i3Qk0xVz+m
Y8DoY8gESKsFwwXQ/pMSrsPBOYVdaqcjrLbEWFjA89lt/mRd/5VlbLTjEc8qn3GQ
gvOgNII35YWLkO1Yq6fjGhWXkZHY3vkw6nXM5MrZiwSj9aBqmi+pl1pB+6qcOvDi
lD3hNzBuLEWMrPKp8y0WDNexq6HAkN7MW59Xa+qqNpCEpQOC+RvVWw+LzHe+N9mP
W/MWFRRlkgb8RVKq71/U72GT8gD6wuGVIlKH7u060iTN68qHmSG5JtvnRfW5AQ0E
WTJC+QEIAMShMj3yOKAyYjytc+qfqFmwJtVsYBZd3ejarJpNRt0t2Ma90xFXr0vB
mnAV5x7ixIso7cM4ZrVcV/RZZbrtgW14Z81ASdVZYHBdCi+ep9N4Uqfp7nVmRUI9
W6YjQFlpDMLZjtFG+B48qW6i2VCVejWt59wpaIYNikPrWmlC2g96QECCrEKt8AAH
jq0+VLgsK7Dok1aVivBDLM4OBGTwd8G9NHyWtfPbvYBgSOy9f+6KViCPkQK+BfUG
v36b1mSpzvbDT3sUA3RzU1ek+te2HaGt5ewGIqHzLqBOl1yHXCoMCAS6aXFyJfN2
kmFufE5pI6gepBT88qtJYlA6qfWwpYkAEQEAAYkBPAQYAQgAJgIbDBYhBKW4oW2G
reyyH7jUiL9I/YT+IdcqBQJc2X4CBQkHaaIJAAoJEL9I/YT+IdcqB2IH/RQdxnMB
alcaT4hPyrQpQ5OOmvv6IC5j550Sj+179RXvILp+r/mxMit2byDKLPJ38xsZeiAZ
FmxZhNii8BTJ6sva7OBLBrymCLWkzazNC1EjNxhDAbLGxRA9zItj1kRt4wVV8h3f
nwymihQQq3PTrNgckejI1pap/1ICkVA0kPuHbDDgmvEwxX1QUPRo8k+9UFsMJ4z1
hKQGIpPG9x4Wrm157MCubXXvL2vRelNSVREFaThpB0g+Ayrn+VGtiF0e5OGwtiOi
p1qpPfXl7FfusylWLMTmnAitFFh8vA+WJOv+ew/Jbp/mk9t/5gSfLt4kyfdMQE0H
b3mLmCLMKo8qD/0=
=MzhL
-----END PGP PUBLIC KEY BLOCK-----
```

This key is valid as of 2017-06-03 with a further renewal done at
2019-05-13. Any older key should be discarded.
{:.warn}

I encourage you to learn how to use encryption for your email
communications and establish it as your default method for trusted
contacts.  You do not need to be a tech wizard to implement it.  Get
started with this excellent guide by the Free Software Foundation on
[email self-defence](https://emailselfdefense.fsf.org/en/).
{:.note}
