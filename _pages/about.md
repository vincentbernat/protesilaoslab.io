---
title: "About me"
excerpt: "About Protesilaos Stavrou."
permalink: /about/
redirect_from:
  - /author
  - /author/
layout: page
---

* Name: Protesilaos (Πρωτεσίλαος)
* Birth place: Greece
* Birth year: 1988
* Email: <public@protesilaos.com> (check the [contact page](https://protesilaos.com/contact/)
  for further information)

## My work does the talking

This website—protesilaos.com—contains more than 600 publications on a
range of topics (all available under the appropriate _copyleft_ terms
that ensure end user freedom—see the [license page](https://protesilaos.com/license/)).  Read,
learn, think, and reach your own conclusions.

What I wrote in the past may not represent my current views.  I review,
change, refine my beliefs as part of a life of research and reflection.
If you are looking for purity, consistency and a dogma to follow, you
may need to find some other resource.

### In English

* [Books and publications](https://protesilaos.com/books/) (philosophy, politics, code)
* [Blog on politics](https://protesilaos.com/politics/) (supersedes my old EU-focused blog)
* [Blog archive](https://protesilaos.com/blog-archive/) (mostly political writings about the EU)
* [Coding blog](https://protesilaos.com/codelog/) (posts about free/libre software)
* [Dotemacs](https://protesilaos.com/dotemacs/) (my configurations for GNU Emacs)
* [Modus Themes](https://protesilaos.com/modus-themes) (highly accessible themes, now in core Emacs)

### In Greek

* [Ιστολόγιο πολιτικής](https://protesilaos.com/greek/) (politics blog)
* [Πρακτικές συμβουλές περί του απλού, φυσικού βίου](https://protesilaos.com/life/) (practical advice for the simple, natural life)
* [Ατάκες και αστειϊσμοί](https://protesilaos.com/jokes/) (jokes, puns, funny experiences)

## Also on

* [GitLab](https://gitlab.com/protesilaos)
* [GitHub](https://github.com/protesilaos)
* [YouTube](https://www.youtube.com/c/ProtesilaosStavrou)

Note that my active git repos are on GitLAB.
