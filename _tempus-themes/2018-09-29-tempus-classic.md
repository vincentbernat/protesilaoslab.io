---
title: "Tempus Classic"
excerpt: "A dark theme with warm hues (WCAG AA compliant). Part of the Tempus Themes project."
date: 2018-09-29
permalink: /tempus-classic/
---

{% include tempus-themes-project-template.md %}
